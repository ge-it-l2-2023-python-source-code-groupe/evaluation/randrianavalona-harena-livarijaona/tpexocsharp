using System;
using tpPoint;
namespace tpCercle;
    public partial class Cercle
        {
           
            public void Display(){
                Console.WriteLine($"CERCLE({Center.Abcisse}, {Center.Ordonne}, {Rayon})");
            }
            // perimetre = 2pi*r
            public string getPerimeter(){
                double p = 2*Math.PI*Rayon;
                string perim = p.ToString("0.00");
                return perim;
            }
            // surface = pi*r^2
            public string getSurface(){
                double s = Math.PI*Rayon*Rayon;
                string surf = s.ToString("0.00");
                return surf; 
            }
            public bool Isinclude(Point p){
                // formule si Point est inclu dans un cercle : racine carre de {(xPoint-xCentre)^2 + (yPoint-yCentre)^2}
                if (Math.Sqrt(Math.Pow(p.Abcisse-Center.Abcisse,2)+Math.Pow(p.Ordonne-Center.Ordonne,2))<=Rayon)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }