using System;
using tpPoint;
namespace tpCercle;
    public partial class Cercle
        {
            public Point Center {get; set;}
            public double Rayon {get; set;}
            
            public Cercle(Point center, double rayon):this(){
                Center=center;
                Rayon=rayon;
            }
            public Cercle(){
                Center=new Point();
                Rayon=1.0;
            }
           
        }