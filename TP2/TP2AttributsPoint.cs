
namespace tpPoint;
    public partial class Point
    {
        public double Abcisse {get; set;}
        public double Ordonne {get; set;}

        
        public Point(double x, double y){
            Abcisse = x;
            Ordonne = y;
        }
        public Point(){
            Abcisse = 0.0;
            Ordonne = 0.0;
        }
        
    }