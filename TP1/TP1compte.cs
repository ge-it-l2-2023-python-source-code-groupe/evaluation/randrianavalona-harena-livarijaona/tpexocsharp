using System;
using tp_client;
namespace tp_compte;

    class Compte
    {
        // Attributs
        private static int compteur = 0; // compteur de comptes
        private int code;
        private double solde;
        private Client proprietaire;

        // Propriétés
        public int Code
        {
            get { return code; }
        }

        public double Solde
        {
            get { return solde; }
            
        }

        public Client Proprietaire
        {
            get { return proprietaire; }
        }

        // Constructeur statique
        static Compte()
        {
            compteur = 0;
        }

        // Constructeur
        public Compte(double solde, Client proprietaire)
        {
            code = ++compteur;
            this.solde = solde;  
            this.proprietaire = proprietaire;
        }
        // Constructeur avec propriétaire (solde initial par défaut)
        public Compte(Client proprietaire)
        {
            code = ++compteur;
            solde = 0; // Solde initial par défaut
            this.proprietaire = proprietaire;
        }
        // Methode créditer
        public void Crediter(double montant)
        {
            solde += montant;
            Console.WriteLine($"Crédit de {montant:C} effectué avec succès. Nouveau solde : {Solde:C}");
        }
        // Methode créditer un compte et debiter un autre
        public void Crediter(double montant, Compte autreCompte)
        {
            solde += montant;
            autreCompte.Debiter(montant);
            
            Console.WriteLine($"Crédit de {montant:C} effectué avec succès. Nouveau solde : {Solde:C}");
            Console.WriteLine($"Débit de {montant:C} effectué sur le compte {autreCompte.Code}. Nouveau solde du compte {autreCompte.Code} : {autreCompte.Solde:C}");
        }
        // Methode Débiter
        public void Debiter(double montant)
        {
            if (solde >= montant)
            {
                solde -= montant;
                Console.WriteLine($"Débit de {montant:C} effectué avec succès. Nouveau solde : {Solde:C}");
            }
            else
            {
                Console.WriteLine("Opération de débit échouée. Solde insuffisant!!!!!!!!!!!");
            }
        }
        // Methode debiter un compte et crediter un autre
        public void Debiter(double montant, Compte autreCompte)
        {
            if (solde >= montant)
            {
                solde -= montant;
                autreCompte.Crediter(montant);
                
                Console.WriteLine($"Débit de {montant:C} effectué avec succès. Nouveau solde : {Solde:C}");
                Console.WriteLine($"Crédit de {montant:C} effectué sur le compte {autreCompte.Code}. Nouveau solde du compte {autreCompte.Code} : {autreCompte.Solde:C}");
            }
            else
            {
                Console.WriteLine("Opération de débit échouée. Solde insuffisant.");
            }
        }


        // Méthode pour afficher les informations du compte
        public void Afficher()
        {
            Console.WriteLine($"Code du compte : {Code}");
            Console.WriteLine($"Solde du compte : {Solde:C}"); // Format de devise
            Console.WriteLine("Propriétaire du compte :");
            proprietaire.Afficher();
        }
        //Methode pour afficher le nombre de comptes créés
        public static void AfficherNombreComptes()
        {
            Console.WriteLine($"Nombre de comptes créés : {compteur}");
        }

    }

