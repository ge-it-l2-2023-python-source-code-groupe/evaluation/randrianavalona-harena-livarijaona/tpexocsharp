﻿using System;
using tp_client;
using tp_compte;

namespace tp_banque;

    class TP1
    {
        public static void ExecuterTP1()
        {
            // Création du compte 1
            Console.Clear();
            Console.WriteLine("Compte 1:");
            Console.Write("Donner Le CIN: ");
            string cin1 = Console.ReadLine();
            Console.Write("Donner Le Nom: ");
            string nom1 = Console.ReadLine();
            Console.Write("Donner Le Prénom: ");
            string prenom1 = Console.ReadLine();
            Console.Write("Donner Le numéro de téléphone: ");
            string tel1 = Console.ReadLine();

            Client client1 = new Client(cin1, nom1, prenom1, tel1);
            Compte compte1 = new Compte(client1);

            Console.WriteLine("Détails du compte:");
            Console.WriteLine("************************");
            compte1.Afficher();
            Console.WriteLine("************************");
            // Opération de crédit sur le compte 1
            Console.Write("Donner le montant à déposer: ");
            double montantCredit1 = Convert.ToDouble(Console.ReadLine());
            compte1.Crediter(montantCredit1);
            Console.WriteLine("************************");
            Console.WriteLine("Détails du compte après opération de crédit:");
            compte1.Afficher();
            Console.WriteLine("************************");
            // Opération de débit sur le compte 1
            Console.Write("Donner le montant à retirer: ");
            double montantDebit1 = Convert.ToDouble(Console.ReadLine());
            compte1.Debiter(montantDebit1);
            Console.WriteLine("************************");
            Console.WriteLine("Détails du compte après opération de débit:");
            compte1.Afficher();
            Console.WriteLine("************************");
            // Création du compte 2
            Console.WriteLine("Compte 2:");
            Console.Write("Donner Le CIN: ");
            string cin2 = Console.ReadLine();
            Console.Write("Donner Le Nom: ");
            string nom2 = Console.ReadLine();
            Console.Write("Donner Le Prénom: ");
            string prenom2 = Console.ReadLine();
            Console.Write("Donner Le numéro de téléphone: ");
            string tel2 = Console.ReadLine();

            Client client2 = new Client(cin2, nom2, prenom2, tel2);
            Compte compte2 = new Compte(client2);

            Console.WriteLine("Détails du compte 2:");
            Console.WriteLine("************************");
            compte2.Afficher();
            Console.WriteLine("************************");
            Console.WriteLine("Opération de crédit sur le compte 2 à partir du compte 1");
            Console.Write("Donner le montant à déposer sur le compte 2: ");
            double montantCredit2 = Convert.ToDouble(Console.ReadLine());
            compte2.Crediter(montantCredit2,compte1);
            Console.WriteLine("************************");
            Console.WriteLine("Détails des comptes après opération de crédit sur le compte 2:");
            compte2.Afficher();
            Console.WriteLine("************************");
            compte1.Afficher();
            Console.WriteLine("************************");
            Console.WriteLine("Opération de débit sur le compte 1 et crédit sur le compte 2");
            Console.Write("Donner le montant à retirer du compte 1 et créditer le compte 2: ");
            double montantDebit2 = Convert.ToDouble(Console.ReadLine());
            compte1.Debiter(montantDebit2, compte2);
            Console.WriteLine("************************");
            Console.WriteLine("Détails des comptes après opération de débit sur le compte 1 et crédit sur le compte 2:");
            compte1.Afficher();
            Console.WriteLine("************************");
            compte2.Afficher();
            Console.WriteLine("************************");

            // Affichage du nombre total de comptes créés
            Compte.AfficherNombreComptes();
            Console.WriteLine("\nAppuyer sur entrer pour revenir au menu principale");
        }
    }

