using System;
using tp_banque;
using tp_cercle;
using jeuxDe;
using HorlogeAlarme;
using menu;
namespace Tp_Csharp;
class Program
{
    static void Main(String[] args){
        //TP1.ExecuterTP1();
        {
            bool quitter = false;

            do
            {
                MenuPrincipale.AfficherMenu();

                // Lire le choix de l'utilisateur
                string choix = Console.ReadLine();

                
                switch (choix)
                {
                    case "1":
                        TP1.ExecuterTP1(); // TP_Banque
                        Console.ReadKey();
                        break;

                    case "2":
                        TP2.ExecuterTP2(); // TP_Cercle
                        Console.ReadKey();
                        break;
                    case "3":
                        TP3.ExecuterTP3(); // TP_JeuDe
                        
                        Console.ReadKey();
                        break;
                    case "4":
                        TP4.ExecuterTP4(); // TP_JeuDe
                        
                        Console.ReadKey();
                        break;

                    case "q":
                        quitter = true;
                        break;

                    default:
                        Console.WriteLine("Choix invalide. Appuyez sur entrer et veuillez réessayer.");
                        Console.ReadKey();
                        break;
                }

            } while (!quitter);
        }
    }
}