using System;
using System.Collections.Generic;
namespace horlogeClass;

    public partial class Horloge
    {
        public static List<Horloge> listeHorloges = new List<Horloge>();
        public void ModifierNomVille(string nouveauNomVille)
        {
            nomVille = nouveauNomVille;
            Console.WriteLine($"Le nom de la ville a été modifié. Nouveau nom : {nomVille}");  
        }
        public void ModifierDecalage(int nouveauDecalage){
            decalageHoraire = nouveauDecalage;
            Console.WriteLine($"Le décalage horaire a été midifié. Nouveau décalage : {decalageHoraire}");

        }
        
        public  void AjouterHorloge(string nomVille, int decalageHoraire)
        {
            Horloge nouvelleHorloge = new Horloge($"{nomVille}", decalageHoraire);
            listeHorloges.Add(nouvelleHorloge);
        }
        public static void AfficherHeuresToutesHorloges()
        {
            foreach (Horloge horloge in listeHorloges)
            {
                Console.WriteLine(horloge.ObtenirHeureFormattee());
            }

        }
        public static string getToutesHorloges()
        {
            string tout = "";
            foreach (Horloge horloge in listeHorloges)
            {
                tout += $"{horloge.ObtenirHeureFormattee()}";
            }
            return tout; 
        }
        public static void AjouterHorlogeList()
            {
                List<string> villes = new List<string> {"Mexique", "Dubai", "Moscou"};
                
                Console.WriteLine("Choisissez une ville parmi les suivantes :");
                Console.WriteLine(string.Join(", ", villes));

                string choixVille = Console.ReadLine() ?? "";

                int decalage = 0;  // Décalage horaire par défaut, si non spécifié

                // Assignez le décalage horaire prédéfini en fonction de la ville choisie
                switch (choixVille.ToLower())
                {
                    case "moscou":
                        decalage = 0;//Même fuseau horaire que Madagascar
                        break;
                    case "dubai":
                        decalage = 1;
                        break;
                    case "mexique":
                        decalage = -6;
                        break;
                    default:
                        Console.WriteLine("Ville non reconnue.");
                        return;  // Quitter la méthode si la ville n'est pas reconnue
                }

                // Créer une instance de Horloge et ajouter à la liste des horloges disponibles
                Horloge nouvelleHorloge = new Horloge(choixVille, decalage);
                Horloge.listeHorloges.Add(nouvelleHorloge);
                Console.WriteLine($"L'horloge pour {choixVille} a été ajoutée avec un décalage horaire de {decalage} heures.");
            }


    }

