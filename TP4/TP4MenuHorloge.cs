using System;
using System.Threading;
using horlogeClass;
namespace Menu
{
    public partial class MenuPrincipal
    {
        Horloge local = new Horloge("Antananarivo", 0);
        public static bool continuerMiseAJour = true;
        public static bool continuerMiseAJourMondial = true;

        
        public void AfficherMenuHorloge()
        {
            Console.Clear();            
            
            Thread heureThread = new Thread(local.AfficherHeureEnTempsReelMenuHorloge);
            
            heureThread.Start();

            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1 - Voir les Horloges actives");
            Console.WriteLine("2 - Ajouter une horloge");
            Console.WriteLine("3 - Retour au menu précédent");
            Console.Write("Quel est votre choix ? ");
            string choix = Console.ReadLine()??"";

            continuerMiseAJour = false;
            heureThread.Join();
            Console.Clear();
            TraiterChoixHorloge(choix);
        }

        private void TraiterChoixHorloge(string choix)
        {
            // traitement des choix du menu Horloge 
            switch (choix)
            {
                case "1":
                    continuerMiseAJourMondial = true;
                    Thread heureThreadMondial = new Thread(local.AfficherHeureEnTempsReelToutesHorloges);
                    Console.Clear();
                    Horloge.AfficherHeuresToutesHorloges();
                    heureThreadMondial.Start();
                    Console.ReadKey();
                    continuerMiseAJourMondial = false;
                    heureThreadMondial.Join();
                    continuerMiseAJour = true;
                    AfficherMenuHorloge();
                    break;
                case "2":
                    Console.Clear();
                    Horloge.AjouterHorlogeList();
                    Console.ReadKey();
                    continuerMiseAJourMondial = true;
                    continuerMiseAJour = true;
                    AfficherMenuHorloge();
                    break;
                case "3":
                    Console.Clear();
                    AfficherMenu();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine($"Choix invalide. Vous avez entré \"{choix}\"\nRemarque : Votre input disparait en une seconde, il n'est pas effacé juste masqué\nDonc si vous avez entré \"1\" par exemple et que cela disparaisse en un instant, veuillez de suite appuyez sur entrer pour valider le \"1\"");
                    Console.ReadKey();
                    continuerMiseAJourMondial = true;
                    continuerMiseAJour = true;
                    AfficherMenuHorloge();
                    break;
            }
        }
    }
}
