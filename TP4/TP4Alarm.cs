using System;

namespace horlogeClass
{
    public partial class Alarm
    {
        public string DateNonPeriodique { get; set; }

        public string NomReference { get; set; }
        public string HeureAlarme { get; set; } = "";
        public string DatePlanification { get; set; } = "";
        public bool LancerUneSeuleFois { get; set; }
        public bool Periodique { get; set; }
        public bool ActiverSonnerieParDefaut { get; set; }
    }
}
