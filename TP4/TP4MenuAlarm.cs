using System;
using horlogeClass;

namespace Menu
{
    public partial class MenuPrincipal
    {
        public void AfficherMenuAlarme()
        {
            Console.Clear();
            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1 - Voir les Alarmes actives");
            Console.WriteLine("2 - Créer une alarme");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");
            string choix = Console.ReadLine()??"";

            TraiterChoixAlarme(choix);
        }

        private void TraiterChoixAlarme(string choix)
        {
            // traitement des choix du menu Alarme 
            switch (choix)
            {
                case "1":
                    Alarm.AfficherAlarmes();
                    Console.ReadKey();
                    AfficherMenuAlarme();
                    break;
                case "2":
                    Alarm.CreerAlarme();
                    Console.ReadKey();
                    AfficherMenuAlarme();
                    break;
                case "3":
                    AfficherMenu();
                    break;
                default:
                    Console.WriteLine("Choix invalide. Appuyez sur une touche pour réessayer.");
                    Console.ReadKey();
                    AfficherMenuAlarme();
                    break;
            }
        }
    }
}
