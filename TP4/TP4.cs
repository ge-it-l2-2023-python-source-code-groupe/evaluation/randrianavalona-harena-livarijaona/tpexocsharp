﻿using System;
using System.Text;
using Menu;
using horlogeClass;
namespace HorlogeAlarme;

    class TP4
    {
        public static void ExecuterTP4()
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            Console.Clear();
            Horloge mondial = new Horloge();
            mondial.AjouterHorloge("New York", -8);
            mondial.AjouterHorloge("Paris", -2);
            mondial.AjouterHorloge("Shanghai", -8);
            
            MenuPrincipal menuPrincipal = new MenuPrincipal
            {
                Quitter = false
            };

            do
            {
                Console.Clear();
                menuPrincipal.AfficherMenu();
                string choix = Console.ReadLine()??"";

            } while (!menuPrincipal.Quitter);
            
            
        }
        
    }

