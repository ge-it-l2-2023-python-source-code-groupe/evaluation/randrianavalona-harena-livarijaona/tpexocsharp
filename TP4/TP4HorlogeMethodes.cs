using System;
using System.Threading;
using Menu;
namespace horlogeClass;

    public partial class Horloge
    {
        public void AfficherHeure()
        {
            DateTime heureLocale = DateTime.Now.AddHours(decalageHoraire);
            Console.WriteLine($"{heureLocale.ToString("HH:mm:ss")}");
            Console.WriteLine($"{nomVille}");
            Console.WriteLine($"{heureLocale.ToString("ddd dd MMM")}");
        }
        
        public void AfficherHeureEnTempsReelMenuHorloge()
        {
            while (MenuPrincipal.continuerMiseAJour)
            {
                Console.Clear(); // pour éviter de décaler les lignes dans la console

                AfficherHeure();
                Console.WriteLine();
                
                Console.WriteLine("Après votre choix appuyez de suite sur entrer");
                Console.WriteLine("Choisissez une option :");
                Console.WriteLine("1 - Voir les Horloges actives");
                Console.WriteLine("2 - Ajouter une horloge");
                Console.WriteLine("3 - Retour au menu principal");
                Console.WriteLine();
                Console.WriteLine("\"Votre input sera masqué en une seconde à cause de l'actualisation de l'heure\"");
                Console.Write("Quel est votre choix ? ");
                

                Thread.Sleep(1000); // Pause d'une seconde avant la prochaine mise à jour
            }
        }
        
        public void AfficherHeureEnTempsReelToutesHorloges()
        {
            string toutesHorloges = getToutesHorloges();
            while (MenuPrincipal.continuerMiseAJourMondial)
            {
                Console.Clear(); 
                Console.Write(toutesHorloges);
                AfficherHeure();
                Console.WriteLine();
                Thread.Sleep(1000); 
            }
        }
        
    
    public string ObtenirHeureFormattee()
    {
        DateTime heureLocale = DateTime.Now.AddHours(decalageHoraire);
        string signeDecalage = (decalageHoraire >= 0) ? "+" : "-";
        
        return $"{nomVille} - {heureLocale.ToString("HH:mm")}\n{signeDecalage}{Math.Abs(decalageHoraire)}h\n";
    }

    }

