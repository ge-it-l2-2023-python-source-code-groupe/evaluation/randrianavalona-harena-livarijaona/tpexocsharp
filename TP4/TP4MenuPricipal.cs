using System;
namespace Menu
{
    public partial class MenuPrincipal
    {
        public bool Quitter {get; set;}
        
        public void AfficherMenu()
        {
            Console.Clear();
            Console.WriteLine("Bienvenue dans le menu principal ( * v * )");
            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1 - Alarme");
            Console.WriteLine("2 - Horloge");
            Console.WriteLine("5 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");
            string choix = Console.ReadLine()??"";

            TraiterChoixPrincipal(choix);
        }

        private void TraiterChoixPrincipal(string choix)
        {
            // traitement des choix du menu principal
            switch (choix)
            {
                case "1":
                    AfficherMenuAlarme();
                    break;
                case "2":
                    continuerMiseAJour = true;
                    AfficherMenuHorloge();
                    break;
                case "5":
                    Quitter = true;
                    Console.WriteLine("Appuyez sur entrer pour quitter");
                    break;
                default:
                    Console.WriteLine("Choix invalide. Appuyez sur une touche pour réessayer.");
                    Console.ReadKey();
                    AfficherMenu();
                    break;
            }
        }
    }
}
