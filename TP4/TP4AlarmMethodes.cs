using System;
using System.Collections.Generic;
namespace horlogeClass
{
    public partial class Alarm
    {
        public static List<Alarm> ListeAlarmes { get; } = new List<Alarm>();
public static void CreerAlarme()
        {
            Alarm nouvelleAlarme = new Alarm();
            Console.WriteLine("Info de la nouvelle Alarme:");

            Console.Write("Donnez un nom de référence : ");
            nouvelleAlarme.NomReference = Console.ReadLine() ?? "";

            Console.Write("Heure de l’alarme (hh:mm) : ");
            nouvelleAlarme.HeureAlarme = Console.ReadLine() ?? "";

            Console.Write("Quand l'alarme est programmée à sonner plus d'une fois c'est classée comme périodique\nLancer une seule fois ? (y/n) : ");
            string reponseLancerUneSeuleFois = Console.ReadLine() ?? "";
            nouvelleAlarme.LancerUneSeuleFois = reponseLancerUneSeuleFois.Equals("y", StringComparison.OrdinalIgnoreCase);

            if (nouvelleAlarme.LancerUneSeuleFois)
            {
                // Si l'alarme est lancée une seule fois, elle est automatiquement non périodique
                nouvelleAlarme.Periodique = false;

                Console.Write("Date de l'alarme pour le lancer une seule fois (jj - mm - aa) : ");
                nouvelleAlarme.DateNonPeriodique = Console.ReadLine() ?? "";
            }
            else
            {
                Console.Write("Les dates de planification de la périodicité (L/M/ME/J/V/S/D) : ");
                nouvelleAlarme.DatePlanification = Console.ReadLine() ?? "";

                Console.Write("Voulez vous que l'on affiche sur l'alarme une mention \"Périodique\" comme rappel ? (y/n) : ");
                string reponsePeriodique = Console.ReadLine() ?? "";
                nouvelleAlarme.Periodique = reponsePeriodique.Equals("y", StringComparison.OrdinalIgnoreCase);
            }

            Console.Write("Activer sonnerie par défaut (y/n) : ");
            string reponseActiverSonnerie = Console.ReadLine() ?? "";
            nouvelleAlarme.ActiverSonnerieParDefaut = reponseActiverSonnerie.Equals("y", StringComparison.OrdinalIgnoreCase);

            ListeAlarmes.Add(nouvelleAlarme);
            Console.WriteLine("Votre nouvelle alarme est enregistrée :)");
        }
        public static void AfficherAlarmes()
        {
            Console.WriteLine("Liste d’alarmes actives:");
            if (ListeAlarmes.Count == 0)
            {
                Console.WriteLine("Aucune alarme créée pour l'instant, veuillez en créer");
            }else
            {
                foreach (Alarm alarme in ListeAlarmes)
            {
                Console.WriteLine($"{alarme.NomReference} - {alarme.HeureAlarme} {(alarme.Periodique ? "(périodique)" : "")}");

                if (alarme.Periodique)
                {
                    Console.WriteLine($"{alarme.DatePlanification}");
                }
                else if (alarme.LancerUneSeuleFois)
                {
                    Console.WriteLine($"{alarme.DateNonPeriodique:ddd dd - MM - yyyy}");
                }
            }
            }

            
        }
    }
}
