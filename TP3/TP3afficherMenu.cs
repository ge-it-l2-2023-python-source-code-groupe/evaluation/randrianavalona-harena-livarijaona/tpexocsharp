using System;
namespace ClassJeux;
public partial class Manche{
    public void AfficherMenu()
        {
            Console.Clear();
            Console.WriteLine("Score actuel :");
            AfficherScore();
            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1. Nouvelle manche");
            Console.WriteLine("2. Voir l'historique");
            Console.WriteLine("3. Retour au Menu principal");
            Console.WriteLine("=============");
        }
}