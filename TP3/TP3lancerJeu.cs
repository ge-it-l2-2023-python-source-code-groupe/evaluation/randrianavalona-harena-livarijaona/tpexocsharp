using System;
namespace ClassJeux;
public partial class Manche
{
     public void EntrerNombreJoueurs()
    {
        bool saisieValide = false;

        while (!saisieValide)
        {
            Console.Write("Entrer le nombre de joueurs : ");
            string userInput = Console.ReadLine();

            if (int.TryParse(userInput, out int result))
            {
                // La conversion en entier a réussi
                nombreJoueurs = result;
                saisieValide = true;
            }
            else
            {
                Console.WriteLine("Veuillez entrer un nombre entier valide.");
            }
        }

        InitialiserScoresCumules();
    }

    public void AjoutNomJoueur()
    {
        int i = 1;        
        while (i<=nombreJoueurs)
        {
            Console.Write($"Entrer le nom du joueur n°{i} :");
            nomDesJoueurs += Console.ReadLine() ?? "";
            nomDesJoueurs += ";";
            scoresdesJoueurs += "0;";
            i++;
        }
    }
    public void AttributionNomJoueur()
    {
        nomIndividuels = nomDesJoueurs.Split(';'); 
        scoreIndividuels = scoresdesJoueurs.Split(';');  
    }
}