using System;
namespace ClassJeux;

public partial class Manche
{
    public void AfficherScore()
    {
        int i = 0;
        while (i < nombreJoueurs)
        {
            Console.WriteLine($"{nomIndividuels[i]} : {scoresCumules[i]} points");
            i++;
        }
    }

    private string[] scoresCumules;

    // les scores cumulés 
    public void InitialiserScoresCumules()
    {
        scoresCumules = new string[nombreJoueurs];
        for (int i = 0; i < nombreJoueurs; i++)
        {
            scoresCumules[i] = "0";
        }
    }

    public void AttributionScore()
    {
        
        faceIndividuels = faceString.Split(';');
        faceString = "";
        scoreIndividuels = scoresdesJoueurs.Split(';');
        int meilleurJet = 0;
        int nombreMeilleursTirages = 0;

        // Trouver le meilleur tirage
        for (int i = 0; i < nombreJoueurs; i++)
        {
            int faceActuelle = Int32.Parse(faceIndividuels[i]);

            // Mettre à jour le meilleur tirage si la face actuelle est meilleure
            if (meilleurJet < faceActuelle)
            {
                meilleurJet = faceActuelle;
                nombreMeilleursTirages = 1;
            }
            else if (meilleurJet == faceActuelle)
            {
                nombreMeilleursTirages++;
            }
        }

        // Attribuer les points en fonction du meilleur tirage et cumuler les scores
        for (int i = 0; i < nombreJoueurs; i++)
        {
            int faceActuelle = Int32.Parse(faceIndividuels[i]);

            // Comparer la face actuelle avec le meilleur tirage
            if (faceActuelle == 6)
            {
                // Le joueur a le meilleur tirage de 6, attribuer 2 points
                if (nombreMeilleursTirages == 1){
                scoresCumules[i] = (Int32.Parse(scoresCumules[i]) + 2).ToString();
                scoreIndividuels[i] = "2";
                } else // plusieurs joueurs ont eu 6
                {
                    scoresCumules[i] = (Int32.Parse(scoresCumules[i]) + 1).ToString();
                    scoreIndividuels[i] = "1";
                }
            }
            
            else
            {
                // Le joueur n'a ni le meilleur tirage de 6 ni le même meilleur tirage que d'autres joueurs, ne gagne pas de points
                scoreIndividuels[i] = "0";
            }
        }
        AjouterMancheAHistorique();
    }
}
