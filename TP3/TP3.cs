﻿using System;
using ClassJeux;
namespace jeuxDe;

class TP3
{
    public static void ExecuterTP3()
    {        
        Console.Clear();
        Manche laManche = new Manche();
        laManche.EntrerNombreJoueurs();
        laManche.AjoutNomJoueur();
        laManche.AttributionNomJoueur();
        laManche.RetourAuMenuPrincipal = true;
        while (laManche.RetourAuMenuPrincipal)
        {
            Console.Clear();
            laManche.AfficherMenu();
            laManche.DeroulementChoix();
        }     
    }    
}
