using System;
namespace ClassJeux;
public partial class Manche
{
    public void JetDe()
    {
        
            Console.Clear();
            quiLeTour = true;
            
            while (quiLeTour)
            {
                int j = 0;
                while(j<nombreJoueurs)
                {
                    int face = 0;
                    Console.WriteLine($"{nomIndividuels[j]} voulez vous lancer le dé ? y/n");
                    string choice = Console.ReadLine() ?? "";
                    if (choice == "y")
                    {
                        face = rand.Next(1, 7);
                    }
                    else if (choice == "n")
                    {
                        face = 0;
                    }
                    else
                    {
                        while(choice !="y" && choice != "n"){
                        Console.WriteLine("Veuillez saisir y ou n");
                         choice = Console.ReadLine() ?? "";
                         if (choice == "y")
                    {
                        face = rand.Next(1, 7);
                    }
                    else if (choice == "n")
                    {
                        face = 0;
                    }

                        }}
                    
                    string faceImage = "";
                    switch (face)
                    {
                        case 0:
                        faceImage = "a passé son tour";
                        break;
                        case 1:
                        faceImage = "|   |\n| * |\n|   |";
                        break;
                        case 2:
                        faceImage = "|*  |\n|   |\n|  *|";
                        break;
                        case 3:
                        faceImage = "|*  |\n| * |\n|  *|";
                        break;
                        case 4:
                        faceImage = "|* *|\n|   |\n|* *|";
                        break;
                        case 5:
                        faceImage = "|* *|\n| * |\n|* *|";
                        break;
                        case 6:
                        faceImage = "|* *|\n|* *|\n|* *|";
                        break;
                        default:
                        break;
                    }
                    Console.WriteLine($"{nomIndividuels[j]}  : \n{faceImage}");
                    j++;
                    faceString += face.ToString();
                    faceString += ";";
                }
               quiLeTour = false;
            }
            
        Console.WriteLine(faceString);
        Console.ReadKey();
    }
}