using System;
namespace ClassJeux;
public partial class Manche{
    public void DeroulementChoix()
    {
            string choix = Console.ReadLine();
            switch (choix)
            {
                case "1":
                    JetDe();
                    AttributionScore();
                    
                    break;
                case "2":
                    Console.Clear();
                    AfficherHistorique();
                    break;
                case "3":
                    RetourAuMenuPrincipal = false;
                    Console.Clear();
                    Console.WriteLine("Appuyer sur entrer pour revenir au menu principale");
                    break;
                default:
                    Console.WriteLine("Option invalide. Appuyez sur une touche pour réessayer.");
                    Console.ReadKey();
                    break;
            }
    }
}