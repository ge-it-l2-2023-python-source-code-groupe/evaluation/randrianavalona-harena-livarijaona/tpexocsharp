using System;
namespace ClassJeux
{
    public partial class Manche
    {
        

        // ajouter une manche à l'historique
        public void AjouterMancheAHistorique()
        {
            string infoManche = $"{DateTime.Now} \nScore actuel :\n";
            for (int i = 0; i < nombreJoueurs; i++)
            {
                infoManche += $"{nomIndividuels[i]}: {scoresCumules[i]} points, ";
            }
            infoManche = infoManche.TrimEnd(',', ' ');

            // les scores individuels à la chaîne
            string infoScoresIndividuels = "Points gagnés lors de cette manche :\n";
            for (int i = 0; i < nombreJoueurs; i++)
            {
                infoScoresIndividuels += $"{nomIndividuels[i]}: {scoreIndividuels[i]} points, ";
            }
            infoScoresIndividuels = infoScoresIndividuels.TrimEnd(',', ' ');

           
            historique += $"{infoManche}\n{infoScoresIndividuels}\n=====================================\n";
        }


        //  méthode pour afficher l'historique
        public void AfficherHistorique()
        {
            Console.Clear();
            Console.WriteLine("Historique des manches :\n===============================================");
            if (historique == "")
            {
                Console.WriteLine("Aucune manche n'a encore été jouée");
            }else
            {
                Console.WriteLine(historique);  
            }
            

            Console.WriteLine("Appuyez sur une touche pour revenir au menu précédent...");
            Console.ReadKey();
        }
    }
}
